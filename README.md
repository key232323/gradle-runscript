# you only need to apply one plugin, then just run script files like this: 

```groovy
buildscript {
	repositories { flatDir { name 'localRepository' } }  
	repositories { localRepository { dirs '../local-repo' } }  

	repositories {
		mavenCentral()
	}

	dependencies {
		classpath 'com.seg.gradle:runscript:1.0-SNAPSHOT'
		classpath 'com.github.kevinsawicki:http-request:5.6'  
	}
}

apply plugin: 'runscript'

runscript {
	// use a remote http mirror
//	runScriptMirror = 'http://localhost:8888'
	runScriptMirror = 'local'
}

task hello(type: com.seg.gradle.RunScriptTask){
	script = 'script1'
	version = '1.1'
	params = [name: 'kerry']
	callback = {
		println it
	}
}
```