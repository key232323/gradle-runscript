package com.seg.gradle

import groovy.transform.CompileStatic

@CompileStatic
class RunScriptException extends Exception {
    RunScriptException(String s) {
        super(s)
    }

    RunScriptException(String s, Exception ex) {
        super(s, ex)
    }
}
