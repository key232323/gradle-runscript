package com.seg.gradle

import groovy.transform.CompileStatic

import org.gradle.api.Plugin
import org.gradle.api.Project

@CompileStatic
class RunScriptPlugin implements Plugin<Project> {
    @Override
    public void apply(final Project project) {
        project.extensions.create('runscript', RunScriptPluginExtension.class)

        project.getTasks().create('runs', RunScriptTask.class)
    }
}