package com.seg.gradle

import groovy.transform.CompileStatic

@CompileStatic
public class RunScriptPluginExtension {

    String runScriptMirror

}  